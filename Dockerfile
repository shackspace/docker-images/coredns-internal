FROM coredns/coredns:latest

#LABEL maintainer="Burke Azbill"

FROM debian:stable-slim

RUN apt-get update && apt-get -uy upgrade
RUN apt-get -y install ca-certificates ucarp bind9utils && update-ca-certificates

# copy coredns executable from coredns container
COPY --from=0 /coredns /coredns

# copy config into container
COPY config/Corefile /etc/coredns/Corefile
COPY dns-repo/bind/pri/db.shack /etc/coredns/db.shack

# copy start script
COPY entrypoint.sh /entrypoint.sh

# copy ifup ifdown scripts
COPY ucarp/vip-down-default.sh /vip-down-default.sh
COPY ucarp/vip-up-default.sh /vip-up-default.sh

# test zone file for syntax errors
RUN named-checkzone shack /etc/coredns/db.shack

EXPOSE 53 53/udp
#VOLUME ["/etc/coredns"]
CMD ["/entrypoint.sh"]
