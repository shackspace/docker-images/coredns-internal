#!/bin/sh

# run ucarp vor virtual ip address management
/usr/sbin/ucarp --interface=${UCARP_INTERFACE} \
                --srcip=${UCARP_SOURCEADDRESS} \
		--advbase=${UCARP_ADVBASE} \
		--advskew=${UCARP_ADVSKEW} \
                --vhid=${UCARP_VHID} \
                --pass=${UCARP_PASS} \
                --addr=${UCARP_VIRTUALADDRESS} \
                --upscript=${UCARP_UPSCRIPT} \
                --downscript=${UCARP_DOWNSCRIPT} \
                --xparam=${UCARP_VIRTUALPREFIX} \
                --nomcast \
                ${UCARP_OPTS} &

# run coredns server
/coredns -conf /etc/coredns/Corefile
