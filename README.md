[![pipeline status](https://gitlab.com/shackhase/docker-images/coredns-internal/badges/master/pipeline.svg)](https://gitlab.com/shackhase/docker-images/coredns-internal/-/pipelines)



Moinsen,

hier soll die Definition für den shack-internen DNS Server entstehen. Das Upstream-Projekt https://github.com/nicolerenee/docker-ucarp wird auch als Template für den "Internet-DNS" Server herhalten.
